from django.db import models
from common.models import Product, Category, Image
from store.models import Store 
import strings
from django.contrib.contenttypes.models import ContentType

# Create your models here.

class Brand(models.Model):
    name = models.CharField( max_length = 100 )
    store_name = models.CharField( max_length = 100, null = True, blank = True )
    description = models.TextField()
    logo_image = models.ForeignKey( Image )
    category   = models.ForeignKey( Category )
    hq_address = models.TextField()
    pincode = models.CharField( max_length = 6 )
    website = models.URLField( null = True )
    fb_link = models.URLField( null = True )
    twitter_link = models.URLField( null = True )
    yt_link = models.URLField( null = True )

    def image_url(self):
        return self.logo_image.get_image_url() \
        if self.logo_image else strings.DEFAULT_BRAND_IMAGE_URL

    def type_id(self):
        return ContentType.objects.get_for_model(self).id

    def __unicode__(self):
        return unicode(self.name)

    def locations(self):
        return [ bs.store.location() for bs in self.brand_store_set.all() ]

    def brand_stores(self):
        return [ bstore for bstore in self.brand_store_set.all() ]

class Brand_product(models.Model):
    brand = models.ForeignKey( Brand )
    product = models.ForeignKey( Product )


class Brand_store(models.Model):
    brand = models.ForeignKey( Brand )
    store = models.ForeignKey( Store )


class Brand_image(models.Model):
    brand = models.ForeignKey( Brand )
    image = models.ForeignKey( Image )