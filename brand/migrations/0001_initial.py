# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Brand'
        db.create_table(u'brand_brand', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('store_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('logo_image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Image'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Category'])),
            ('hq_address', self.gf('django.db.models.fields.TextField')()),
            ('pincode', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('fb_link', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('twitter_link', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('yt_link', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
        ))
        db.send_create_signal(u'brand', ['Brand'])

        # Adding model 'Brand_product'
        db.create_table(u'brand_brand_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['brand.Brand'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Product'])),
        ))
        db.send_create_signal(u'brand', ['Brand_product'])

        # Adding model 'Brand_store'
        db.create_table(u'brand_brand_store', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['brand.Brand'])),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'])),
        ))
        db.send_create_signal(u'brand', ['Brand_store'])


    def backwards(self, orm):
        # Deleting model 'Brand'
        db.delete_table(u'brand_brand')

        # Deleting model 'Brand_product'
        db.delete_table(u'brand_brand_product')

        # Deleting model 'Brand_store'
        db.delete_table(u'brand_brand_store')


    models = {
        u'brand.brand': {
            'Meta': {'object_name': 'Brand'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'fb_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'hq_address': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pincode': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'twitter_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'yt_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'})
        },
        u'brand.brand_product': {
            'Meta': {'object_name': 'Brand_product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['brand.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Product']"})
        },
        u'brand.brand_store': {
            'Meta': {'object_name': 'Brand_store'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['brand.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"})
        },
        u'common.category': {
            'Meta': {'object_name': 'Category'},
            'category_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'common.image': {
            'Meta': {'object_name': 'Image'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'common.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'area_sqft': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'contact_no': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'days_closed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'floor_no': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'manager_contact'", 'null': 'True', 'to': u"orm['common.Contact']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'owner_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owner_contact'", 'null': 'True', 'to': u"orm['common.Contact']"}),
            'payment_method': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop_no': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tags': ('tagging.fields.TagField', [], {})
        }
    }

    complete_apps = ['brand']