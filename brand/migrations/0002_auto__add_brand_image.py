# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Brand_image'
        db.create_table(u'brand_brand_image', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['brand.Brand'])),
            ('image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Image'])),
        ))
        db.send_create_signal(u'brand', ['Brand_image'])


    def backwards(self, orm):
        # Deleting model 'Brand_image'
        db.delete_table(u'brand_brand_image')


    models = {
        u'brand.brand': {
            'Meta': {'object_name': 'Brand'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'fb_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'hq_address': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo_image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pincode': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'twitter_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'}),
            'yt_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'})
        },
        u'brand.brand_image': {
            'Meta': {'object_name': 'Brand_image'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['brand.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']"})
        },
        u'brand.brand_product': {
            'Meta': {'object_name': 'Brand_product'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['brand.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Product']"})
        },
        u'brand.brand_store': {
            'Meta': {'object_name': 'Brand_store'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['brand.Brand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"})
        },
        u'common.category': {
            'Meta': {'object_name': 'Category'},
            'category_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'common.image': {
            'Meta': {'object_name': 'Image'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'common.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'area_sqft': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'contact_no': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'days_closed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'floor_no': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'manager_contact'", 'null': 'True', 'to': u"orm['common.Contact']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'owner_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owner_contact'", 'null': 'True', 'to': u"orm['common.Contact']"}),
            'payment_method': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop_no': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tags': ('tagging.fields.TagField', [], {})
        }
    }

    complete_apps = ['brand']