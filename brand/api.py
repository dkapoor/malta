from tastypie.resources import ModelResource
from brand.models import *
from tastypie import fields
from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.http import HttpUnauthorized, HttpForbidden, HttpApplicationError
from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields
from tastypie.models import ApiKey, create_api_key
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import models
from common.models import Image, Contact, City, Country, Product, Activity
from store.models import Store
from common.api import CategoryResource, ImageResource, urlencodeSerializer, ProductResource, AdminApiKeyAuthentication
from store.api import StoreResource
import ast

from log import get_request_logger
logger = get_request_logger()


class BrandResource(ModelResource):
    logo_image = fields.ForeignKey( ImageResource, 'logo_image', full = True )
    category = fields.ForeignKey( CategoryResource, 'category', full = True )
    type_id = fields.IntegerField( attribute = 'type_id' )
    
    class Meta:
        queryset = Brand.objects.all()
        allowed_methods = ['get', 'post', 'delete']
        resource_name = 'brand'
        filtering = {
            'id': 'exact',
            'city_id': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

    def obj_create( self, bundle, request = None, **kwargs ):
        bundle = super( BrandResource, self ).obj_create( bundle, request = request, **kwargs )
        brand = bundle.obj
        request = bundle.request

        store_ids = request.REQUEST.get( 'brand_store_ids', None )
        if store_ids:
            store_ids = ast.literal_eval( store_ids )
            stores = Store.objects.filter( id__in = store_ids )
            for store in stores:
                Brand_store.objects.create( brand = brand, store = store )

        product_ids = request.REQUEST.get( 'brand_product_ids', None )
        if product_ids:
            product_ids = ast.literal_eval( product_ids )
            products = Product.objects.filter( id__in = product_ids )
            for product in products:
                Brand_product.objects.create( brand = brand, product = product )
        image_ids = request.REQUEST.get('brand_image_ids')
        if image_ids:
            brand_image_ids = ast.literal_eval( image_ids )
            [ bi.delete() for bi in Brand_image.objects.filter( brand = brand ) ]
            images = Image.objects.filter( id__in = brand_image_ids )
            for image in images:
                Brand_image.objects.create( brand = brand, image = image )
        return bundle


    def authorized_read_list(self, object_list, bundle):
        if bundle.request.REQUEST.has_key('city_id'):
            city_id = int( bundle.request.REQUEST.get('city_id') )
            ol = []
            for obj in object_list:
                locations = obj.locations()
                for location in locations:
                    if location and location.city.id == city_id:
                        ol.append(obj)
            object_list = ol
        if len(object_list) == 1 and bundle.request.REQUEST.has_key('api_key'):
            user = ApiKey.objects.get( key = bundle.request.REQUEST['api_key']).user
            Activity.note_activity('VI', object_list[0], user = user )
        return super( BrandResource, self ).authorized_read_list( object_list, bundle )

    def dehydrate(self, bundle):
        city_ids = []
        if bundle.obj.locations():
            locations = set(bundle.obj.locations())
            for loc in locations:
                city_ids.append( loc.city.id ) if ( loc and loc.city.id not in city_ids ) else None
        bundle.data['city_ids'] = city_ids
        return super( BrandResource, self ).dehydrate( bundle )

class BrandProductResource(ModelResource):
    brand = fields.ForeignKey( BrandResource, 'brand', full = True )
    product = fields.ForeignKey( ProductResource, 'product', full = True )

    class Meta:
        queryset = Brand_product.objects.all()
        allowed_methods = [ 'get', 'delete' ]
        resource_name = 'brand_product'
        filtering = {
            'brand': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()


class BrandStoreResource(ModelResource):
    brand = fields.ForeignKey( BrandResource, 'brand', full = True )
    store = fields.ForeignKey( StoreResource, 'store', full = True )

    class Meta:
        queryset = Brand_store.objects.all()
        allowed_methods = [ 'get', 'delete' ]
        resource_name = 'brand_store'
        filtering = {
            'brand': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()

class BrandImageResource(ModelResource):
    brand = fields.ForeignKey( BrandResource, 'brand', full = True )
    image = fields.ForeignKey( ImageResource, 'image', full = True )

    class Meta:
        queryset = Brand_image.objects.all()
        allowed_methods = [ 'get', 'delete' ]
        resource_name = 'brand_image'
        filtering = {
            'brand': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()



        