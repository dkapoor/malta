import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'malta.settings'

import_paths = [ '/usr/local/malta' ]
for path in import_paths:
    if path not in sys.path:
       sys.path.append(path)

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

os.environ['CELERY_LOADER'] = 'django'