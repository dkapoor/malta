from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.contenttypes.models import ContentType

def send_html_email( subject, to_email, html_template, template_vars_dict, from_email = None ):
    
    html_content = render_to_string(html_template, template_vars_dict)
    text_content = strip_tags(html_content) 
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def get_object_from_ct( ct_id, object_id ):
    ct = ContentType.objects.get_for_id(ct_id)
    return ct.get_object_for_this_type(pk=object_id) 

def get_avg_rating( votes ):
    avg = 0.0
    total_votes = votes.count()
    if total_votes:        
        for vote in votes:
            avg += vote.rating_value
        avg = (avg/total_votes)
    return avg

def create_activity_stream( activities ):
    activities_list = []
    lookup = {'VI': 'Viewed', 'RE': 'Commented on', 'RA': 'Rated'}
    for activity in activities:
        data = {}
        action_object = activity.content_type.get_object_for_this_type(pk = activity.object_id)
        data['activity_str'] = "%s %s %s"%( activity.user.username, lookup[ activity.action ], unicode(action_object) )
        data['image_url'] = action_object.image_url()
        activities_list.append( data )
    return activities_list
