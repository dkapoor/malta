from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from datetime import datetime
from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from log import get_request_logger
logger = get_request_logger()
from django import forms
from django.conf import settings
from django.core.management import call_command
from django.db.models.signals import post_save
from geoposition.fields import GeopositionField
import os
from tastypie.models import create_api_key
from django.contrib.auth.models import User
import uuid, urllib
import utils, strings
# Create your models here.



models.signals.post_save.connect(create_api_key, sender=User)

class User_profile(models.Model):
    USER_ROLES = (
            ('ru', 'Regular User'),
            ('mm', 'Mall Manager'),
            ('sm', 'Store Manager'),
            ('sa', 'Site Admin'),
        )
    user = models.ForeignKey( User, null = True )
    firstname = models.CharField( max_length = 50 )
    lastname = models.CharField( max_length = 50  )
    role = models.CharField( max_length = 2, choices = USER_ROLES )
    email = models.EmailField( unique = True )
    is_verified = models.BooleanField( default = False )

    def is_profile_complete( self ):
        logger.debug('( self.firstname %s self.lastname %s self.role %s self.email ) %s' \
         %(( self.firstname, self.lastname, self.role, self.email )))
        return ( self.firstname and self.lastname and self.role and self.email )

    def is_role_valid( self, role ):
        return ( role in [ user_role[0] for user_role in USER_ROLES ] )
    def __unicode__(self):
        return unicode(self.user) + ' profile'
    
    def get_user_options(self, user):
        options = {'My Profile': reverse('common.views')}
        if self.role == 'ru':
            return 

    def save( self, *args, **kwargs):
        if self.id and self.email != User_profile.objects.get(id = self.id ).email:
            send_email( self )
        super(User_profile, self).save(*args, **kwargs)


class Profile_verification(models.Model):
    guid = models.CharField( max_length = 100 )
    profile = models.ForeignKey( User_profile )

class Image(models.Model):
    image = models.ImageField( upload_to = 'images', null = True )
    name  = models.CharField( max_length = 100, null = True, blank = True )
    description = models.TextField( null = True, blank = True )
    
    def __unicode__(self):
        return self.name if self.name else "Image Not Named"

    def save(self, *args, **kwargs):
        if not self.name and self.image:
            self.name = os.path.basename ( self.image.file.name )
        elif not self.name:
            self.name = "Image Not Named"
        super( Image, self ).save( *args, **kwargs )

    def delete(self, *args, **kwargs):
        try:
            if self.image:
                os.remove( self.image.file.name )
        except Exception,e:
            logger.exception('exception deleting file: %s' %(e))

        super( Image, self ).delete(*args, **kwargs)

    def get_image_url(self):
        return self.image.url
    

class Category(models.Model):

    CATEGORY_TYPES = (
                    ('EVE', 'Event'),
                    ('OFF', 'Offer'),
                    ('SHO', 'Shop'),
                    ('BRD', 'Brand'),
        )

    name = models.CharField(max_length = 50)
    description = models.TextField( null = True, blank = True)
    category_type = models.CharField( max_length = 3, choices = CATEGORY_TYPES )

    def __unicode__(self):
        return unicode(self.name) + '_' + unicode(self.category_type)

class RatingCategory(models.Model):
    name = models.CharField(max_length = 50)
    description = models.TextField( null = True, blank = True)
    rating_min = models.PositiveIntegerField( default = 0 )
    rating_max = models.PositiveIntegerField( default = 5 )

    def __unicode__(self):
        return self.name        

class EntityRatingCategoryMap(models.Model):
    category = models.ForeignKey(RatingCategory)
    entity_type_id = models.PositiveIntegerField()    

    def __unicode__(self):
        return unicode( self.category ) + '_' + unicode( self.entity_type_id )

    @classmethod
    def get_categories(cls, entity_type ):
        return EntityRatingCategoryMap.objects.filter( entity_type = entity_type )

class RatingVote(models.Model):
    rating_category = models.ForeignKey(EntityRatingCategoryMap)
    content_type = models.ForeignKey( ContentType )    
    object_id = models.PositiveIntegerField()
    rated_object = GenericForeignKey('content_type', 'object_id')
    rating_value = models.PositiveIntegerField()
    rated_by = models.ForeignKey(User)
    rated_on = models.DateTimeField( auto_now_add = True )
    
    @classmethod
    def get_vote_details( cls, entity, entity_map, request ):
        votes = RatingVote.objects.filter( rating_category = entity_map, \
                                        content_type = ContentType.objects.get_for_model(entity), object_id = entity.id )
        return_list = []
        if not ( request.user.is_authenticated and votes.filter( rated_by = request.user.id ).exists() ):
            return_list.append( 0 )
        else:
            return_list.append( 1 )

        avg = 0.0
        total_votes = votes.count()
        if total_votes:        
            for vote in votes:
                avg += vote.rating_value
            avg = (avg/total_votes)
        return return_list + [ total_votes, int(avg) ]

    @classmethod
    def avg_rating(cls, rating_category, content_type, object_id):
        votes = RatingVote.objects.filter( rating_category = rating_category, \
                                        content_type = content_type, object_id = object_id )
        return utils.get_avg_rating( votes )

    @classmethod
    def overall_avg_rating( cls, entity ):
        ratings = RatingVote.objects.filter( content_type = ContentType.objects.get_for_model(entity), object_id = entity.id )
        avg = 0.0
        total_votes = ratings.count()
        if not total_votes:
            return None
        for r in ratings:
            avg += r.rating_value
        avg = (avg/total_votes)        
        return ("%.1f/5"%(avg))

    @classmethod
    def add_vote( cls, entity_map, content_type, object_id, rating_value, user ):
        already_rated = False
        rate = None
        if RatingVote.objects.filter( rating_category = entity_map, content_type = content_type, object_id = object_id, rated_by = user).exists():
            already_rated = True
            rate = RatingVote.objects.filter( rating_category = entity_map, content_type = content_type, object_id = object_id, rated_by = user)[0]
        else:
            rate = RatingVote( rating_category = entity_map, content_type = content_type, object_id = object_id, rated_by = user )
        
        if int( rating_value ) < entity_map.category.rating_min or \
            int( rating_value ) > entity_map.category.rating_max:
            raise Exception('Rating value not correct!')
        rate.rating_value = int( rating_value )
        rate.save()
        return (rate, already_rated )

class Comment( models.Model ):
    text = models.TextField()
    post_date = models.DateTimeField( auto_now_add = True )
    posted_by = models.ForeignKey( User )
    content_type = models.ForeignKey( ContentType )
    object_id = models.PositiveIntegerField()
    comment_object = GenericForeignKey('content_type', 'object_id')
    is_spam = models.BooleanField( default = False )

    @classmethod
    def post_comment(cls, content_type_id, object_id, user, comment ):
        logger.debug('user: %s' %user)
        content_type = ContentType.objects.get( id = content_type_id )
        entity = content_type.get_object_for_this_type( pk = object_id )
        comment = Comment( text = comment, posted_by = user, content_type = content_type, object_id = entity.id )
        comment.save()
        return comment

    @classmethod
    def get_comments( cls, content_type_id, object_id ):
        content_type = ContentType.objects.get( id = content_type_id )
        return Comment.objects.filter( content_type = content_type, object_id = object_id, is_spam = False ).order_by('-post_date')        
        

class Contact(models.Model):
    name = models.CharField( max_length = 100 )
    phone = models.CharField( max_length = 20, null = True, blank = True )
    email = models.EmailField( null = True, blank = True )

    def save(self, *args, **kwargs):
        if self.phone and not self.phone.isdigit():
            raise Exception("phone number is in improper format, please check")
        super( Contact, self ).save( *args, **kwargs )

    def __unicode__(self):
        return unicode(self.name) + '_' + unicode(self.phone)


class Activity(models.Model):
    ACTION_ITEMS = (
            ('VI', 'viewed'),
            ('RA', 'Rated'),
            ('RE', 'Commented'),
            
        )
    user = models.ForeignKey( User, null = True )
    action = models.CharField( max_length = 2, choices = ACTION_ITEMS )
    content_type =  models.ForeignKey( ContentType )
    object_id = models.PositiveIntegerField()
    entity = GenericForeignKey('content_type', 'object_id')
    action_time = models.DateTimeField( default = datetime.now )
    weightage = models.PositiveIntegerField()

    @classmethod
    def note_activity(cls, action, entity, user = None ):
        weightage = Activity_weightage.objects.get( activity_type = action ).weightage
        activity = Activity( action = action, \
        content_type = ContentType.objects.get_for_model(entity), object_id = entity.id, weightage = weightage )
        if user:
            activity.user = user 
        activity.save()
    
    @classmethod    
    def get_latest_activity(cls, min_limit = 0, max_limit = 10 ):
        activities = list( Activity.objects.all().order_by('-id')[ min_limit: max_limit ] )
        activities.sort( key = lambda x: x.weightage )
        return activities


class Activity_weightage(models.Model):
    activity_type = models.CharField( max_length = 2, choices = Activity.ACTION_ITEMS )
    weightage = models.PositiveIntegerField()

    def __unicode__(self):
        return unicode(self.activity_type) + '_' + unicode(self.weightage)

def comment_save_handler( sender, instance, **kwargs):
    
    if kwargs.get('created', None):
        entity = instance.content_type.get_object_for_this_type( pk = instance.object_id )
        Activity.note_activity( 'RE', entity,user = instance.posted_by )

def vote_save_handler( sender, instance, **kwargs):
    
    if kwargs.get('created', None):
        entity = instance.content_type.get_object_for_this_type( pk = instance.object_id )
        Activity.note_activity( 'RA', entity, user = instance.rated_by )

def generate_verification_link( guid ):
    return (strings.EMAIL_VERIFY_ROOT_URL) + urllib.urlencode( {'guid': guid} )

def send_email( instance ):
    guid = str( uuid.uuid1() )
    verification = Profile_verification( guid = guid, profile = instance )
    verification.save()
    utils.send_html_email( strings.EMAIL_VERIFY_TITLE, instance.email, strings.VERIFICATION_EMAIL_TEMPLATE, {'verification_link': generate_verification_link( verification.guid ) } )



def send_verification_email( sender, instance, **kwargs ):
    if ( kwargs.get('created', None) and not instance.is_verified ):
        send_email( instance )
        
post_save.connect( comment_save_handler, sender = Comment, dispatch_uid = "comment_activity" )
post_save.connect( vote_save_handler, sender = RatingVote, dispatch_uid = "rating_activity" )
post_save.connect( send_verification_email, sender = User_profile, dispatch_uid = "profile_verification")

class Country(models.Model):
    name = models.CharField( max_length = 50 )

    def __unicode__(self):
        return unicode(self.name)  

class State(models.Model):
    name = models.CharField( max_length = 50 )
    country = models.ForeignKey( Country )

    def __unicode__(self):
        return unicode(self.name)
        

class City(models.Model):
    name = models.CharField( max_length = 50 )
    state = models.ForeignKey( State )

    def __unicode__(self):
        return unicode(self.name)

class Contact(models.Model):
    name = models.CharField( max_length = 100 )
    number = models.CharField( max_length = 12 )

    def __unicode__(self):
        return unicode(self.name) 

class Social_link(models.Model):
    fb_link = models.URLField( null = True )
    yt_link = models.URLField( null = True )
    twitter_link = models.URLField( null = True )
        

class Timing(models.Model):

    HOUR_CHOICES = tuple(zip([ str(i) for i in range(0,13)], range(0,13)))
    MIN_CHOICES = tuple(zip([ str(i) for i in range(0,60)], range(0,60)))
    AM_PM_CHOICES = (
            ("AM", "AM"),
            ("PM", "PM"),
        )

    start_hour = models.CharField( max_length = 2, choices = HOUR_CHOICES )
    start_min = models.CharField( max_length = 2, choices = MIN_CHOICES )
    start_am_pm  =  models.CharField( max_length = 2, choices = AM_PM_CHOICES )
    end_hour = models.CharField( max_length = 2, choices = HOUR_CHOICES )
    end_min = models.CharField( max_length = 2, choices = MIN_CHOICES )
    end_am_pm  =  models.CharField( max_length = 2, choices = AM_PM_CHOICES )
  
        
class Location(models.Model):
     name  = models.CharField( max_length = 100 )
     city     = models.ForeignKey( City )

     def __unicode__(self):
        return unicode(self.name) + '_' + unicode(self.city)
        

class Product(models.Model):
    title = models.CharField( max_length = 100 )
    image = models.ForeignKey( Image, null = True, blank = True )
    category = models.ForeignKey( Category, null = True, blank = True )
    description = models.TextField( null = True, blank = True )

    def __unicode__(self):
        return unicode(self.title)

    def type_id(self):
        return ContentType.objects.get_for_model(self).id


class Data:
    pass
        
