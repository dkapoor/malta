from common.models import *
from mall.models import Mall
from store.models import Store
from tastypie import fields
from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
import os, re
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.http import HttpUnauthorized, HttpForbidden, HttpApplicationError
from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields
from tastypie.models import ApiKey, create_api_key
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import models
import urlparse
from tastypie.serializers import Serializer
from utils import get_object_from_ct, create_activity_stream

from log import get_request_logger
logger = get_request_logger()

from tastypie.exceptions import Unauthorized

from tastypie.authentication import ApiKeyAuthentication
from tastypie import http
from tastypie.exceptions import ImmediateHttpResponse

PWD_MIN = 8
PWD_MAX = 15
PWD_REGEX = ( "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{%s,%s}$" % ( PWD_MIN, PWD_MAX ) )

class AdminApiKeyAuthentication(ApiKeyAuthentication):
    def is_authenticated(self, request, **kwargs):        
        if request.method == 'GET':
            return True
        apikey = request.REQUEST.get('api_key', None)
        username = request.REQUEST.get( 'username', None )
        if apikey and username:
            user = ApiKey.objects.get(key = apikey).user
            return ( ( user.username == username and user.is_authenticated() ) and ( (request.method in [ 'POST', 'DELETE'] and user.is_superuser) ) )
        return False


# class DjangoAdminAuthorization(DjangoAuthorization):

#     def create_detail(self, object_list, bundle):
#         result = super(DjangoAdminAuthorization, self).create_detail(object_list, bundle)

#         # now we check here for specific permission
#         if not bundle.request.user.is_superuser:
#             raise Unauthorized("You are not allowed to create this resource.")

#         return result

#     def delete_detail(self, object_list, bundle):
#         result = super(DjangoAdminAuthorization, self).delete_detail(object_list, bundle)

#         # now we check here for specific permission
#         if not bundle.request.user.is_superuser:
#             raise Unauthorized("You are not allowed to delete this resource.")

#         return result


class MultiPartResource(object):
    def deserialize(self, request, data, format=None):
       if not format:
           format = request.Meta.get('CONTENT_TYPE', 'application/json')
       if format == 'application/x-www-form-urlencoded':
           return request.POST
       if format.startswith('multipart'):
           data = request.POST.copy()
           data.update(request.FILES)
           return data
       return super(MultiPartResource, self).deserialize(request, data, format)


    def put_detail(self, request, **kwargs):
        if request.META.get('CONTENT_TYPE').startswith('multipart') and \
                not hasattr(request, '_body'):
            request._body = ''

        return super(MultipartResource, self).put_detail(request, **kwargs)

class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        #'multipart': 'multipart/form-data',
        }
    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self,content): 
        pass

    def format_date(self, data):
        return data.strftime("%d-%m-%Y")



class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        allowed_methods = ['get', 'post']
        resource_name = 'user'
        always_return_data = True
        filtering = {
            'username': 'exact'
        }

    def build_filters( self, filters=None ):
        if filters is None:
            filters = {}

        orm_filters = super(UserResource, self).build_filters( filters )

        #If we need to serve the field username in User, the username in authentication info acts as a filter key. Removing it here.
        if orm_filters.has_key( "username__exact" ):
            orm_filters.pop( "username__exact" )
        else:
            #Do not serve unfiltered requests
            orm_filters[ 'pk' ] = "-1"

        return orm_filters

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name='api_login'),
            url(r"^(?P<resource_name>%s)/logout%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
            url(r"^(?P<resource_name>%s)/create%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create'), name='api_create'),
        ]

    def login(self, request, **kwargs):
        logger.debug( request )
        self.method_check(request, allowed=['post'])
        try:
            data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))
            username = data.get('username', '')
            password = data.get('password', '')
        except:
            username = request.POST.get('username', '')
            password = request.POST.get('password', '')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)

                try:
                    key = ApiKey.objects.get(user=user)
                except ApiKey.DoesNotExist:
                    return self.create_response( request, { 'success': False, 'reason': 'missing key' }, HttpForbidden )

                return self.create_response( request, { 'success': True, 'username': user.username, 'key': key.key, 'is_superuser': user.is_superuser } )
            else:
                return self.create_response( request, { 'success': False, 'reason': 'disabled' }, HttpForbidden )
        else:
            return self.create_response( request, { 'success': False, 'reason': 'invalid login', 'skip_login_redir': True }, HttpUnauthorized )

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated( request )
        if request.user:
            logout(request)
            return self.create_response(request, {'success': True})
        else:
            return self.create_response(request, {'success': False}, HttpUnauthorized)
    
    def check_password( self, pwd, confirm ):
        regex = re.compile(PWD_REGEX)        
        return ( pwd == confirm ) and ( regex.match( pwd ) and regex.match( confirm ) )

    def create(self, request, **kwargs):
        logger.debug( request )
        self.method_check(request, allowed=['post'])
        try:  
            email = request.REQUEST.get('email')
            firstname = request.REQUEST.get('firstname')
            lastname = request.REQUEST.get('lastname')

            username = request.REQUEST.get('username')
            password = request.REQUEST.get('password')
            confirm = request.REQUEST.get('confirm')
                
            if not self.check_password( password, confirm ):
                raise Exception('Password criteria doesnt match')

            user = User.objects.create_user(username = username, email = email, password = password )
            profile = User_profile( user = user, email = email, firstname = firstname, lastname = lastname, role = 'ru' )
            profile.save()
            
            return self.create_response( request, { 'success': True, 'user_id': user.id } )            
            
        except Exception, e:
            logger.exception('exception while creating user: %s' %(e))
            return self.create_response( request, { 'success': False, 'reason': ('%s' %(e)) }, HttpApplicationError )
            
    
class ImageResource( MultiPartResource, ModelResource ):
    image = models.ImageField( 'image', null = True, blank = True )
    class Meta:
        queryset = Image.objects.all()
        resource_name = 'image'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
        }

        limit = 0
        always_return_data = True
        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def obj_create(self, bundle, **kwargs):
        api_key = bundle.request.REQUEST.get('api_key')
        if not ApiKey.objects.get( key = api_key ).user.is_superuser:
            e = Exception('Forbidden.')
            raise ImmediateHttpResponse(response=http.HttpUnauthorized(e.message))
        if not bundle.request.FILES.has_key('image'):
            e = Exception('Image not provided')
            raise ImmediateHttpResponse(response=http.HttpApplicationError(e.message))
        return super( ImageResource, self ).obj_create(bundle, **kwargs)

class ContactResource(ModelResource):
    class Meta:
        queryset = Contact.objects.all()
        resource_name = 'contact'
        allowed_methods = ['get', 'post']
        filtering = {
            'id': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()


class CategoryResource(ModelResource):
    class Meta:
        queryset = Category.objects.all()
        resource_name = 'category'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': ALL_WITH_RELATIONS,
            'category_type': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

class CountryResource(ModelResource):
    class Meta:
        queryset = Country.objects.all()
        resource_name = 'country'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact'
        }
        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

        
class StateResource(ModelResource):
    country = fields.ForeignKey( CountryResource, 'country', full = True  )
    class Meta:
        queryset = State.objects.all()
        resource_name = 'state'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'country_id': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

class CityResource(ModelResource):
    state = fields.ForeignKey( StateResource, 'state', full = True )
    class Meta:
        queryset = City.objects.all()
        resource_name = 'city'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'state_id': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()


class ProductResource(ModelResource):
    image = fields.ForeignKey( ImageResource, 'image', full = True, null = True )
    category = fields.ForeignKey( CategoryResource, 'category', full = True, null = True )
    type_id = fields.IntegerField( attribute = 'type_id' )
    
    class Meta:
        queryset = Product.objects.all()
        resource_name = 'product'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'category_id': 'exact'
        }
        
        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()


# class OfferResource(ModelResource):
#     offer_object = GenericForeignKeyField({
#         Store: StoreResource,
#         Mall: MallResource,
#         Image: ImageResource,
#     }, 'offer_object')
#     class Meta:
#         queryset = Offer.objects.all()
#         resource_name = 'offer'
#         allowed_methods = ['get', 'post']
#         filtering = {
#             'id': 'exact'
#         }

#         authorization = Authorization()
#         serializer = urlencodeSerializer()
    
#     def build_filters(self, request, *args, **kwargs):
#         content_type = request.POST.get['content_type' ]
#         app_label = 'common' if content_type == 'image' else content_type
#         print app_label + ',' + content_type
#         print request.POST.get['object_id']
#         content_type = ContentType.objects.get( app_label = app_label, model = content_type )


class TimingResource(ModelResource):
    class Meta:
        queryset = Timing.objects.all()
        resource_name = 'timing'
        allowed_methods = ['get']
        filtering = {
            'id': 'exact'
        }
        
        authorization = ReadOnlyAuthorization()

class UserProfileResource(ModelResource):
    class Meta:
        queryset = User_profile.objects.all()
        resource_name = 'user_profile'
        excludes = ('role')
        allowed_methods = ['get', 'post']
        filtering = {
        'user': ALL_WITH_RELATIONS,
        }
        always_return_data = True

        authentication = ApiKeyAuthentication()
        authorization = Authorization()

    def prepend_urls( self ):
        return [
        url(r"^(?P<resource_name>%s)/verify_email%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('verify_email'), name='api_verify_email'),
        ]

    def obj_create(self, bundle, request = None, **kwargs):
        user = bundle.request.user
        bundle = super( UserProfileResource, self ).obj_create( bundle, request = request, **kwargs )
        bundle.obj.user = user        
        bundle.obj.save()        
        
        return bundle

    def verify_email( self, request, **kwargs ):
        logger.debug( request )
        self.method_check(request, allowed=['get'])
        try:            
            guid = request.REQUEST.get( 'guid', None )
            profile_verification = Profile_verification.objects.get( guid = guid )
            profile_verification.profile.is_verified = True
            profile_verification.profile.save()

            return self.create_response( request, { 'success': True } )
        except Exception, e:
            logger.exception('exception while verification: %s' %(e))
            return self.create_response( request, { 'success': False, 'reason': ('%s' %(e)) }, HttpApplicationError )
            

class LocationResource(ModelResource):
    city = fields.ForeignKey( CityResource, 'city', null = True, full = True )
        
    class Meta:
        queryset = Location.objects.all()
        resource_name = 'location'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'city': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()


class RatingCategoryResource(ModelResource):
        
    class Meta:
        queryset = RatingCategory.objects.all()
        resource_name = 'rating_category'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

class EntityRatingCategoryMapResource(ModelResource):
    entity_type_id = fields.IntegerField( attribute = 'entity_type_id' )
    class Meta:
        queryset = EntityRatingCategoryMap.objects.all()
        resource_name = 'rating_map'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'entity_type_id': 'exact',
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

    def obj_create(self, bundle, **kwargs):
        request = bundle.request
        ct_id = request.REQUEST.get('entity_type_id')
        if not ContentType.objects.filter( id = ct_id ).exists():
            return HttpApplicationError({'error': 'entity_type_id not correct!'} )
        return super( EntityRatingCategoryMapResource, self ).obj_create( bundle, **kwargs )


class RatingVoteResource(ModelResource):

    class Meta:
        queryset = RatingVote.objects.all()
        resource_name = 'rating'
        allowed_methods = ['get', 'post', 'delete']
    
        always_return_data = True
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()  

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/rate%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('rate'), name='api_rate'),
            url(r"^(?P<resource_name>%s)/avg_rating%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('avg_rating'), name='avg_rating'),
        ]


    @classmethod
    def extract_user_data( cls, request ):
        data = Data()
        data.entity_map = EntityRatingCategoryMap.objects.get(id = request.REQUEST['entity_map_id'] )
        data.content_type = ContentType.objects.get( id = request.REQUEST['type_id'] )
        data.object_id = request.REQUEST['id']
        return data

    def rate(self, request, **kwargs):
        logger.debug( request )
        self.is_authenticated( request )
        self.method_check(request, allowed=['post'])
        already_rated = False
        try:
            data = RatingVoteResource.extract_user_data( request )
            rating = request.REQUEST['rating_value']
            rate, already_rated = RatingVote.add_vote( data.entity_map, data.content_type, data.object_id, rating, request.user )
            avg_rating = RatingVote.avg_rating(data.entity_map, data.content_type, data.object_id)
            rate_resource = RatingVoteResource()
            rating_bundle = rate_resource.build_bundle( obj = rate, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'already_rated': already_rated, 'avg_rating': ("%.1f" % (avg_rating)), 'rate': rate_resource.full_dehydrate( rating_bundle ) } )

        except Exception, e:
            logger.exception('exception while rating: %s' %(e))
            return self.create_response( request, { 'success': False, 'already_rated': already_rated, 'reason': ('%s' %(e)) }, HttpApplicationError )
    
    def avg_rating(self, request, **kwargs):
        logger.debug( request )
        self.method_check(request, allowed=['get'])
        avg_rating = 0.0
        try:

            data = RatingVoteResource.extract_user_data( request )
            avg_rating = RatingVote.avg_rating(data.entity_map, data.content_type, data.object_id)
            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'avg_rating': ("%.1f" % (avg_rating)) } )
        
        except Exception, e:
            logger.exception('exception while avg_rating: %s' %(e))
            return self.create_response( request, { 'success': False, 'avg_rating': ("%.1f" % (avg_rating)), 'reason': ('%s' %(e)) }, HttpApplicationError )    


class CommentResource(ModelResource):
    username = fields.CharField( attribute="posted_by__username" )

    class Meta:
        queryset = Comment.objects.all().order_by('-post_date')
        resource_name = 'commenting'
        allowed_methods = ['get', 'post', 'delete']

        filtering = {
            'id': 'exact',
            'content_type': ALL_WITH_RELATIONS,
            'object_id': 'exact',
        }
    
        always_return_data = True
        authorization = Authorization()
        serializer = urlencodeSerializer()  

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/comment%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('comment'), name='api_comment'),
        ]

    def comment(self, request, **kwargs):
        logger.debug( request )
        self.is_authenticated( request )
        self.method_check(request, allowed=['post'])
        already_rated = False
        try:
            user = ApiKey.objects.get(key = request.REQUEST['api_key']).user
            ct_id = request.REQUEST['type_id']
            object_id = request.REQUEST['id']
            text = request.REQUEST['comment']
            comment = Comment.post_comment(ct_id, object_id, user, text)
            comment_resource = CommentResource()
            comment_bundle = comment_resource.build_bundle( obj = comment, request = request )

            self.log_throttled_access( request )
            return self.create_response( request, { 'success': True, 'comment': comment_resource.full_dehydrate( comment_bundle ) } )
        
        except Exception, e:
            logger.exception('exception while comment: %s' %(e))
            return self.create_response( request, { 'success': False, 'reason': ('%s' %(e)) }, HttpApplicationError )    


class ActivityResource(ModelResource):

    class Meta:
        queryset = Activity.objects.all()
        resource_name = 'activity'
        allowed_methods = ['get']

        filtering = {
            'id': 'exact',
        }
    
        always_return_data = True
        authorization = Authorization()
        serializer = urlencodeSerializer()  

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/stream%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('stream'), name='api_stream'),
        ]

    def stream( self, request, **kwargs):
        logger.debug( request )
        self.method_check(request, allowed=['get'])
        min_limit = int( request.REQUEST['min_limit'] ) if request.REQUEST.has_key('min_limit') else 0
        max_limit = int( request.REQUEST['max_limit'] ) if request.REQUEST.has_key('max_limit') else 10
        activities = Activity.get_latest_activity(min_limit = min_limit, max_limit = max_limit )
        stream_data = create_activity_stream( activities )

        return self.create_response( request, { 'success': True, 'stream': stream_data } )
