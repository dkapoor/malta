from django import template
from django.contrib.contenttypes.models import ContentType

register = template.Library()

@register.filter
def content_type(obj):
    if not obj:
        return False
    return ContentType.objects.get_for_model(obj)

@register.filter
def get_activity_entity(obj):
    if not obj:
        return None
    retval = obj.content_type.get_object_for_this_type( pk = obj.object_id )
    return retval