# # Create your views here.
# from django.contrib.auth.decorators import login_required
# from django.shortcuts import render_to_response
# from django.http import HttpResponse
# from django.template import RequestContext
# from models import User_profile, EntityRatingCategoryMap, RatingVote, Comment
# from forms import ProfileForm, UserForm
# from log import get_request_logger
# logger = get_request_logger()
# from django.contrib.auth.hashers import make_password
# from django.core.urlresolvers import reverse
# from django.contrib.auth import login, authenticate
# from django.shortcuts import redirect  
# from django.utils import simplejson 
# from django.core.urlresolvers import reverse
# from django.contrib.auth.models import User
# from mall.models import *
# from store.models import *
# from geoposition.fields import GeopositionField
# from django.contrib.contenttypes.models import ContentType
# from common.models import Address, Contact


# def sign_up( request, **kwargs ):
#     logger.debug( request )
#     if request.method == 'GET': 
#         role = request.GET.get( 'role', 'ru' )
#         logger.debug('role: %s' %(role))
#         form = UserForm( initial ={ 'role': role })
#         return render_to_response('common/profile_form.html',{'form' : form}, context_instance=RequestContext(request))
#     else:
#         form = UserForm( request.POST )
#         if form.is_valid():
#             username = form.cleaned_data[ 'username' ]
#             email = form.cleaned_data[ 'email' ]
#             password = form.cleaned_data[ 'password' ]
#             user = User.objects.create( username = username, email = email, password = make_password( password ) )
#             user_data = form.save( commit = False )
#             user_data.user = user           
#             user_data.save()
#             user = authenticate( username = user.username, password = password )
#             login( request, user )
#             return redirect( reverse('post-login') )
#         else:
#             return render_to_response('common/profile_form.html',{'form' : form}, context_instance=RequestContext(request))


# def get_response( user, profile, context = {} ):
#     if profile.role == 'ru':
#         return render_to_response('common/ru_feed.html', context, context_instance = RequestContext(request) )
#     elif profile.role == 'sm':
#         return render_to_response('common/store_manager.html', context, context_instance = RequestContext(request) )
#     elif profile.role == 'mm':
#         return render_to_response('common/mall_manager.html', context, context_instance = RequestContext(request) )
#     else:
#         return HttpResponse( 'Illegal role passed.' )

# @login_required
# def post_login( request, **kwargs ):
#     logger.debug( request )
#     try:
#         profile = request.user.get_profile()
#     except:
#         profile = User_profile( user = request.user )
#         profile.save()
#     if not profile.is_profile_complete():
#         form = UserForm(instance = profile)
#         return render_to_response('common/profile_form.html',{'form' : form}, context_instance=RequestContext(request))
#     else:
#         return redirect( reverse('home') )

# def put_rating(request, map_id, ctype_id, obj_id, rating ):

#     retval = {'success': False }
#     if request.is_ajax() and request.user.is_authenticated:
#         cat_map = EntityRatingCategoryMap.objects.get( id = map_id )

#         if not RatingVote.objects.filter(rating_category = cat_map, content_type__id = ctype_id, object_id = obj_id, rated_by = request.user.id ).exists():        
#             ctype = ContentType.objects.get( id = ctype_id )
#             rating = RatingVote( rating_category = cat_map, content_type = ctype, object_id = obj_id, rating_value = rating, rated_by = request.user )
#             rating.save()
#             retval[ 'success' ] = True
#     return HttpResponse(simplejson.dumps(retval))

# def post_comment(request):
#     if request.method == "POST" and request.user.is_authenticated:
#         content_type_id = request.POST.get('content_type', None)
#         object_id = request.POST.get('object_id', None)
#         comment_text = request.POST.get('comment', None)
#         comment, entity = Comment.post_comment( content_type_id, object_id, request.user, comment_text )
#         if isinstance(entity, Mall):
#             url = reverse('mall_view', kwargs = {'id': entity.id, 'slug': entity.slug })
#         if isinstance(entity, Store):
#             url = reverse('store_view', kwargs = {'id': entity.id, 'slug': entity.slug })
#     return redirect( url )

# @login_required
# def create( request, entity_type ):
#     if request.method == "GET":
#         malls = Mall.objects.all() if entity_type == "store" else None
#         return render_to_response('common/create_entity_form.html', { 'malls': malls, 'entity_type': entity_type }, context_instance=RequestContext(request))
#     else:
#         name = request.POST[ "name" ]
#         owned_by = request.POST[ "owned_by" ]
#         description = request.POST[ "description" ]
#         address = request.POST.get("street", None)
#         nearby = request.POST.get("nearby", None)
#         phone = request.POST.get("phone", None)
#         email = request.POST.get("email", None)

#         address = Address( street = address, nearby = nearby )
#         address.save()

#         contact = Contact( phone = phone, email = email )
#         contact.save()

#         if entity_type == "mall":
#             lat = request.POST[ "latitude" ]
#             longitude = request.POST[ "longitude" ]
#             mall = Mall.objects.create( name = name )
#             mall_profile = Mall_profile( mall = mall, owned_by = owned_by, description = description )
#             mall_profile.address = address
#             mall_profile.location = lat + ',' + longitude
#             mall_profile.address = address
#             mall_profile.mall_contact = contact
#             mall_profile.save()
#             mall_profile.manager.add( request.user )
#             mall_profile.save()
#             return redirect( reverse('mall_list') )
        
#         elif entity_type == "store":
#             mall_id = request.POST.get( "mall", None )
#             store_profile = Store_profile( owner = request.user,store_contact = contact, store_address = address )
#             store_profile.save()
#             store = Store(name = name, description = description, profile = store_profile)
#             store.save()
#             if mall_id:
#                 mall = Mall.objects.get( id = mall_id )
#                 mall_store_mapping = Mall_store( mall = mall, store = store )
#                 mall_store_mapping.save()
#                 return redirect( reverse('store_list') )
 
#     