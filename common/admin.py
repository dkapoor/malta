from django.contrib import admin
from models import *

admin.site.register(Image)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(State)
admin.site.register(Category)
admin.site.register(Timing)
admin.site.register(User_profile)
admin.site.register(Location)

