from django.forms import ModelForm
from models import User_profile
from django.contrib.auth.models import User
from django import forms
from django.core.validators import validate_slug, MinLengthValidator, MaxLengthValidator, RegexValidator
from log import get_request_logger
logger = get_request_logger()
from mall.models import Mall_profile
from gmapi.forms.widgets import GoogleMap

class UserForm(ModelForm): 
    username = forms.CharField(widget=forms.TextInput(attrs = {'class':'form-control', 'placeholder':'Username'}), validators = [ validate_slug, MinLengthValidator(5), MaxLengthValidator(20) ] )
    password = forms.CharField( widget = forms.PasswordInput(attrs = {'class':'form-control', "placeholder": "Enter Password"}), validators = [ validate_slug, MinLengthValidator(7), MaxLengthValidator(30) ] )
    retype_password = forms.CharField( widget = forms.PasswordInput(attrs = {'class':'form-control', "placeholder": "Retype Password"}), validators = [ validate_slug, MinLengthValidator(7), MaxLengthValidator(30) ] )
    email    = forms.EmailField(widget=forms.TextInput(attrs = {'class':'form-control', "placeholder": "Your Email", "type": "email" } ))
    firstname = forms.CharField(widget=forms.TextInput(attrs = {'class':'form-control', "placeholder" : "First Name"}), validators = [ RegexValidator("[a-zA-Z]+", "First Name is invalid.") ] )
    lastname = forms.CharField(widget=forms.TextInput(attrs = {'class':'form-control', "placeholder" : "Last Name"}), validators = [ RegexValidator("[a-zA-Z]+", "Last Name is invalid.") ] )
    role = forms.CharField( widget = forms.HiddenInput() )

    class Meta:
        model = User_profile
        exclude = ( 'user', 'is_verified' )

    def clean(self, *args, **kwargs ):
        super( UserForm, self ).clean(*args, **kwargs)
        
        if User.objects.filter( username = self.cleaned_data.get('username') ).exists():
            raise forms.ValidationError("User %s already exists." %(self.cleaned_data.get('username')))
        
        password = self.cleaned_data.get('password')
        retype =  self.cleaned_data.get('retype_password')
        
        if not ( password and retype ) or ( password != retype ):
            raise forms.ValidationError("Passwords don't match")
        
        return self.cleaned_data


class ProfileForm(ModelForm):    
    class Meta:
        model = User_profile
        fields = ( 'firstname', 'lastname', 'email' )

class MapForm(forms.Form):
    map = forms.Field(widget=GoogleMap(attrs={'width':210, 'height':210}))
