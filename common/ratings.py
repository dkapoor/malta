from mall.models import Mall
from store.models import Store
from django.contrib.contenttypes.models import ContentType
from models import EntityRatingCategoryMap, RatingVote


def get_rating_dict( entity, request ):	
    rating_categories = EntityRatingCategoryMap.get_categories( entity.get_type() )
    rating_dict = {}
    for rating_category in rating_categories:
        rating_dict[ rating_category.category.name ] = [ int(rating_category.id) ] + RatingVote.get_vote_details( entity, rating_category, request )
    return rating_dict