EMAIL_VERIFY_TITLE = "Verify your Email Address"
VERIFICATION_EMAIL_TEMPLATE = 'email_verification.html'
DEFAULT_STORE_IMAGE_URL = "http://marketsandmalls:8000/media/images/default_store.jpg"
DEFAULT_MALL_IMAGE_URL = "http://marketsandmalls:8000/media/images/default_mall.jpg"
DEFAULT_BRAND_IMAGE_URL = "http://marketsandmalls:8000/media/images/default_brand.jpg"
EMAIL_VERIFY_ROOT_URL = "http://marketsandmalls.com:8000/api/v1/user_profile/verify_email/?"
DATE_TIME_FORMAT = "%d/%m/%Y %H:%M:%S"
WHITESPACES = ",| "