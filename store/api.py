from tastypie.resources import ModelResource
from store.models import *
from datetime import datetime
from strings import DATE_TIME_FORMAT
from tastypie import fields
from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
import os
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.http import HttpUnauthorized, HttpForbidden, HttpApplicationError
from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields
from tastypie.models import ApiKey, create_api_key
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import models
from common.api import urlencodeSerializer, ImageResource, CategoryResource, ProductResource, \
 TimingResource, ContactResource, UserResource, AdminApiKeyAuthentication, CityResource
from mall.api import MallResource
from tastypie.contrib.contenttypes.fields import GenericForeignKeyField
from tastypie.exceptions import BadRequest
from common.models import Comment, Product, RatingCategory, EntityRatingCategoryMap, RatingVote, City, Location, Activity
from brand.models import Brand
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.core.urlresolvers import resolve
import ast, operator, re
from itertools import chain
from common.api import LocationResource

from log import get_request_logger
logger = get_request_logger()
from django.db.models import Q

class StoreResource(ModelResource):
    owner_contact = fields.ForeignKey( ContactResource, 'owner_contact', full= True, null = True )
    manager_contact = fields.ForeignKey( ContactResource, 'manager_contact', full = True, null = True )
    type_id = fields.IntegerField( attribute = 'type_id' )
    brand_name = fields.CharField( attribute = 'brand_name', null = True, blank = True )
    category = fields.ForeignKey( CategoryResource, 'category', full = True )
    class Meta:
        queryset = Store.objects.all()
        resource_name = 'store'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'mall_id': 'exact',
            'category':ALL_WITH_RELATIONS,
            'brand_name':'exact',
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

    def authorized_read_list(self, object_list, bundle):
        if bundle.request.REQUEST.has_key('mall_id'):
            mall_id = int( bundle.request.REQUEST.get('mall_id') )
            object_list = [ obj for obj in object_list if obj.mall() and obj.mall().id == mall_id ]
        if len(object_list) == 1 and bundle.request.REQUEST.has_key('api_key'):
            user = ApiKey.objects.get( key = bundle.request.REQUEST['api_key']).user
            Activity.note_activity('VI', object_list[0], user = user )

        return super( StoreResource, self ).authorized_read_list( object_list, bundle )

    def dehydrate(self, bundle):
        if bundle.obj.mall():
            bundle.data['mall_id'] = bundle.obj.mall().id
        return super( StoreResource,self ).dehydrate( bundle )

    def obj_create( self, bundle, request = None, **kwargs ):
        bundle = super( StoreResource, self ).obj_create( bundle, request = request, **kwargs )
        store = bundle.obj
        request = bundle.request

        #Dictionary of name: number
        owner_contact = request.REQUEST.get('owner_contact_dict', None )
        
        if owner_contact:
            owner_contact = ast.literal_eval( owner_contact )
            if store.owner_contact:
                store.owner_contact.delete()
            store.owner_contact = Contact.objects.create( name = owner_contact.keys()[0], phone = owner_contact.values()[0] )

        mgr_contact = request.REQUEST.get('manager_contact_dict', None )
        
        if mgr_contact:
            mgr_contact = ast.literal_eval( mgr_contact )
            if store.manager_contact:
                store.manager_contact.delete()
            store.manager_contact = Contact.objects.create( name = mgr_contact.keys()[0], phone = mgr_contact.values()[0] )     

        tags = request.REQUEST.get('tags', None )   
        
        if tags:
            store.tags = tags
        store.save()
        
        if request.REQUEST.get('mall_id', None):
            mall = Mall.objects.get( id = request.REQUEST[ 'mall_id' ] )
            mall_store, created = Mall_store.objects.get_or_create(mall = mall, store = store )
        
        store_image_ids = request.REQUEST.get('store_image_ids', None)
        thumbnail_image_id = request.REQUEST.get( 'thumbnail_image_id', 0 )

        if store_image_ids:
            store_image_ids = ast.literal_eval( store_image_ids )
            store_images = Image.objects.filter(id__in = store_image_ids )
            result = [ si.store_image.delete() or si.delete() for si in Store_image.objects.filter( store = store ) ]
            for si in store_images:
                sin = Store_image( store = store, store_image = si )
                sin.is_thumbnail = (si.id == int( thumbnail_image_id ) )
                sin.save()

        timing = request.REQUEST.get('store_timing', None)

        if timing:
            timing = ast.literal_eval( timing )
            start_hour   = timing.get('start_hour', None)
            start_min   =  timing.get('start_min', None)
            end_hour   = timing.get('end_hour', None)
            end_min   = timing.get('end_min', None)
            start_am_pm   = timing.get('start_am_pm', None)
            end_am_pm   = timing.get('end_am_pm', None)
        
            old_store_timings = Store_timing.objects.filter( store = store )
            
            if old_store_timings.exists():
                result = [ old_store_timing.delete() for old_store_timing in old_store_timings ]
                
            timing, created = Timing.objects.get_or_create( start_hour = start_hour, start_min = start_min, \
                                                                end_hour = end_hour, end_min = end_min, start_am_pm = start_am_pm.upper(), end_am_pm = end_am_pm.upper() )

            store_timing = Store_timing.objects.create( store = store, timing = timing )

        product_ids = request.REQUEST.get('store_product_ids', None)
        
        if product_ids:
            product_ids = ast.literal_eval( product_ids )
            products = Product.objects.filter(id__in = product_ids)
            for product in products:
                store_product = Store_product.objects.create(store = store, product = product )
        
        return bundle


class MallStoreResource(ModelResource):
    mall = fields.ForeignKey( MallResource, 'mall', full = True )
    store = fields.ForeignKey( StoreResource, 'store', full = True )

    class Meta:
        queryset = Mall_store.objects.all()
        resource_name = 'mall_store'
        allowed_methods = [ 'get' ]
        filtering = {
            'id': 'exact',
            'mall': ALL_WITH_RELATIONS,
            'store': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()

class StoreImageResource(ModelResource):
    store = fields.ForeignKey( StoreResource, 'store', full = True )
    store_image = fields.ForeignKey( ImageResource, 'store_image', full = True )
    
    class Meta:
        queryset = Store_image.objects.all()
        resource_name = 'store_image'
        allowed_methods = ['get']
        filtering = {
            'id': 'exact',
            'store': ALL_WITH_RELATIONS
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()

class StoreTimingResource(ModelResource):
    store = fields.ForeignKey( StoreResource, 'store', full = True )
    timing = fields.ForeignKey( TimingResource, 'timing', full = True )

    class Meta:
        queryset = Store_timing.objects.all()
        resource_name = 'store_timing'
        allowed_methods = ['get']
        filtering = {
            'id': 'exact',
            'store': ALL_WITH_RELATIONS
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()

class StoreProductResource(ModelResource):
    store = fields.ForeignKey( StoreResource, 'store', full = True )
    product = fields.ForeignKey( ProductResource, 'product', full = True )

    class Meta:
        queryset = Store_product.objects.all()
        resource_name = 'store_product'
        allowed_methods = ['get']
        filtering = {
            'id': 'exact',
            'store': ALL_WITH_RELATIONS
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()

class StoreOfferResource(ModelResource):
    store = fields.ForeignKey( StoreResource, 'store', full = True )
    image = fields.ForeignKey( ImageResource, 'image', full = True, null = True )
    category = fields.ForeignKey( CategoryResource, 'category', null = True, full = True )

    class Meta:
        queryset = Store_offer.objects.all()
        resource_name = 'store_offer'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'store': ALL_WITH_RELATIONS,
            'category': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer  = urlencodeSerializer()

    def hydrate_is_active(self, bundle):
        is_active = bundle.data.get( "is_active", None )
        bundle.data["is_active"] = ( is_active.lower() == "true" )
        return bundle

    def authorized_read_list(self, object_list, bundle):
        if bundle.request.REQUEST.has_key('mall_id'):
            mall_id = int( bundle.request.REQUEST.get('mall_id') )
            object_list = [ obj for obj in object_list if obj.store.mall() and obj.store.mall().id == mall_id ]

        return super( StoreOfferResource, self ).authorized_read_list( object_list, bundle )

    def dehydrate(self, bundle):
        if bundle.obj.store.mall():
            bundle.data['mall_id'] = bundle.obj.store.mall().id
        return super( StoreOfferResource,self ).dehydrate( bundle )

class EventTypeResource(ModelResource):
    class Meta:
        queryset = Eventtype.objects.all()
        resource_name = 'event_type'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()


class EventResource(ModelResource):
    image = fields.ForeignKey( ImageResource, 'image', null = True, full = True )
    category = fields.ForeignKey( CategoryResource, 'category', null = True, full = True )
    event_type = fields.ForeignKey( EventTypeResource, 'event_type', full = True, null = True )
    store = fields.ForeignKey( StoreResource, 'store', full = True )
    type_id = fields.IntegerField( attribute = 'type_id' )
  
    class Meta:
        queryset = Event.objects.all()
        resource_name = 'event'
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'id': 'exact',
            'mall_id': 'exact',
            'category': ALL_WITH_RELATIONS,
            'store': ALL_WITH_RELATIONS,
            'event_type': ALL_WITH_RELATIONS
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

    def obj_create( self, bundle, request = None, **kwargs ):
        bundle = super( EventResource, self ).obj_create( bundle, request = request, **kwargs )
        start_str = bundle.request.REQUEST.get('start_time')
        end_str = bundle.request.REQUEST.get('end_time')
        if not start_str:
            raise Exception("start_time not provided.")
        if not datetime.strptime( start_str, strings.DATE_TIME_FORMAT ):
            raise Exception("start_time format not correct.")
        if end_str and ( not datetime.strptime( end_str, strings.DATE_TIME_FORMAT ) ):
            raise Exception("end_time format not correct.")
        bundle.obj.start_time = datetime.strptime( start_str, strings.DATE_TIME_FORMAT )
        bundle.obj.end_time = datetime.strptime( end_str, strings.DATE_TIME_FORMAT ) if end_str else None
        bundle.obj.save()
        return bundle

    def authorized_read_list(self, object_list, bundle):
        if bundle.request.REQUEST.has_key('mall_id'):
            mall_id = int( bundle.request.REQUEST.get('mall_id') )
            object_list = [ obj for obj in object_list if obj.store.mall() and obj.store.mall().id == mall_id ]

        return super( EventResource, self ).authorized_read_list( object_list, bundle )

    def dehydrate(self, bundle):
        if bundle.obj.store.mall():
            bundle.data['mall_id'] = bundle.obj.store.mall().id
        return super( EventResource,self ).dehydrate( bundle )

# class CommentResource(ModelResource):
#     from brand.api import BrandResource
#     posted_by = fields.ForeignKey( UserResource, 'posted_by', full = True )
#     comment_object = GenericForeignKeyField({
#         Mall: MallResource,
#         Brand: BrandResource,
#         Store: StoreResource,
#         Product: ProductResource,

#     }, 'comment_object')
#     class Meta:
#         queryset = Comment.objects.all()
#         resource_name = 'comment'
#         allowed_methods = ['get', 'post']
#         filtering = {
#             'id': 'exact',
#             'content_type': ALL_WITH_RELATIONS,
#             'object_id': 'exact',
#         }
    
#         limit = 0
#         always_return_data = True
#         authentication = ApiKeyAuthentication()
#         authorization = Authorization()
#         serializer = urlencodeSerializer()

#     def obj_create( self, bundle, request = None, **kwargs ):
#         posted_by = bundle.request.user if bundle.request.user.is_authenticated() else User.objects.all()[0]
#         view, args, kwargs = resolve(bundle.data.get('comment_object', None) )
#         comment = bundle.data.get('text', None)
#         content_type = ContentType.objects.get( model = kwargs['resource_name'] )
#         object_id = int( kwargs['pk'] )
#         obj, entity = Comment.post_comment( content_type.id, object_id, posted_by, comment )
#         new_bundle = self.build_bundle( obj = obj, request = request )
#         return new_bundle

# class RatingCategoryResource(ModelResource):
    
#     class Meta:
#         queryset = RatingCategory.objects.all()
#         resource_name = 'rating_category'
#         allowed_methods = ['get', 'post']
#         filtering = {
#             'id': 'exact',
#             'content_type': ALL_WITH_RELATIONS,
#             'object_id': 'exact',
#         }
    
#         limit = 0
#         always_return_data = True
#         authentication = ApiKeyAuthentication()
#         authorization = Authorization()
#         serializer = urlencodeSerializer()
        

# class EntityRatingCategoryMapResource(ModelResource):
#     category = fields.ForeignKey( RatingCategoryResource, 'category', full = True )

#     class Meta:
#         queryset = EntityRatingCategoryMap.objects.all()
#         resource_name = 'category_map'
#         allowed_methods = ['get', 'post']
#         filtering = {
#             'id': 'exact',
#             'entity_type': 'exact',
#         }
    
#         limit = 0
#         always_return_data = True
#         authentication = AdminApiKeyAuthentication()
#         authorization = Authorization()
#         serializer = urlencodeSerializer()

# class RatingVoteResource(ModelResource):
#     category = fields.ForeignKey( EntityRatingCategoryMapResource, 'rating_category', full = True )
#     rated_by = fields.ForeignKey( UserResource, 'rated_by', full = True )
#     avg_rating = fields.FloatField( attribute = 'avg_rating', null = True )

#     class Meta:
#         queryset = RatingVote.objects.all()
#         resource_name = 'rating_vote'
#         allowed_methods = ['get', 'post']
#         filtering = {
#             'id': 'exact',
#             'entity_type': 'exact',
#             'category': ALL_WITH_RELATIONS,
#         }
    
#         limit = 0
#         always_return_data = True
#         authentication = ApiKeyAuthentication()
#         authorization = Authorization()
#         serializer = urlencodeSerializer()

#     def prepend_urls(self):
#         return [
#                 url(r"^(?P<resource_name>%s)/avg_rating%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view( 'avg_rating' ), name = "api_avg_rating" ),
#              ]


class SearchObject(object):

    def __init__(self, id=None, name=None, obj_type=None, description = None, image_url = None, avg_rating = None, address = None, **kwargs ):
        self.id = id
        self.name = name
        self.obj_type = obj_type
        self.description = description
        self.image_url = image_url
        self.avg_rating = avg_rating
        self.address = address

class SearchResource(Resource):
    id = fields.CharField(attribute='id')
    name = fields.CharField(attribute='name')
    obj_type = fields.CharField(attribute='obj_type')
    description = fields.CharField(attribute = 'description')
    image_url = fields.CharField(attribute = 'image_url')
    avg_rating = fields.CharField( attribute = 'avg_rating', null = True )
    address = fields.CharField( attribute = 'address', null = True )

    class Meta:
        resource_name = 'search'
        object_class = SearchObject
        allowed_methods = ['get']
        include_resource_uri = False

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.id
        else:
            kwargs['pk'] = bundle_or_obj['id']

        return kwargs

    def get_object_list(self, bundle, **kwargs):
        query = bundle.request.REQUEST.get('q', None)
        if not query:
            raise BadRequest("Missing query parameter")

        city_id = bundle.request.REQUEST.get('city_id', None)
        location_ids = bundle.request.REQUEST.get('location_ids', None)
        category_ids = bundle.request.REQUEST.get('category_ids', None)
        location_filter = ( location_ids is not None )
        category_filter = ( category_ids is not None )

        queries =  [ q for q in re.split( strings.WHITESPACES, query ) if q ]
        mall_query = reduce( operator.or_, (Q(description__icontains=query) | Q(name__icontains=query) | Q(address__icontains=query) for query in queries ))
        malls = Mall.objects.filter( mall_query )
        store_query = reduce( operator.or_, (Q(description__icontains=query) | Q(name__icontains=query) for query in queries ))
        stores = Store.objects.filter( store_query )
        brand_query = reduce( operator.or_, ( Q(description__icontains=query) | Q(name__icontains=query)  | Q(store_name__icontains=query)  for query in queries ) )
        brands = Brand.objects.filter( brand_query )
        brand_stores = []
        for brand in brands:
            bstores = brand.brand_stores()
            brand_stores += bstores
        brand_stores = list( set( brand_stores + list(stores) ) )

        if location_ids:
            self.locations = list( Location.objects.filter(id__in = location_ids.split(',') ) )
        else:
            self.locations = list( set( [ mall.location for mall in malls ] + [ store.location() for store in brand_stores if store.location() ] ) )
        if city_id:
            self.city = City.objects.get( id = city_id )
            self.locations = [ location for location in self.locations if location.city == city ]
        
        if location_filter:
            malls = malls.filter( location__in = self.locations )
            brand_stores = [ store for store in brand_stores if store.location() in self.locations ]

        if category_ids:
            self.categories = list( Category.objects.filter( id__in = category_ids.split(',') ) )
        else:
            self.categories = list( set([ brand.category for brand in brands ] + [ store.category for store in brand_stores ] ) )

        if category_filter:
            brands = [ brand for brand in brands if brand.category in self.categories ]
            brand_stores = [ store for store in brand_stores if store.category in self.categories ]

        objects = list( chain(malls, stores, brands) )
        result = []
        
        for obj in objects:
            result_obj = SearchObject( id = obj.id, name = obj.name, description = obj.description, obj_type = obj.__class__.__name__, image_url = obj.image_url(), avg_rating = RatingVote.overall_avg_rating( obj ), \
                                       address = ( obj.address if isinstance(obj, Mall) else None ) )
            result.append(result_obj)

        return result

    def obj_get_list(self, bundle, **kwargs):
        return self.get_object_list(bundle, **kwargs) 

    def create_response(self, request, output, **kwargs):
        locs = []
        for location in self.locations:
            location_resource = LocationResource()
            loc_bundle = location_resource.build_bundle( obj = location, request = request )
            locs.append( location_resource.full_dehydrate( loc_bundle ) )
        output[ "locations" ] = locs
        cats = []
        for category in self.categories:
            category_resource = CategoryResource()
            cat_bundle = category_resource.build_bundle( obj = category, request = request )
            cats.append( category_resource.full_dehydrate( cat_bundle ) )
        output[ "categories" ] = cats
        if hasattr(self, 'city') and self.city:
            city_resource = CityResource()
            city_bundle = city_resource.build_bundle( obj = self.city, request = request )
            output['city'] = city_resource.full_dehydrate( city_bundle )
        return super( SearchResource, self ).create_response( request, output, **kwargs )
   