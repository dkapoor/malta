# # Create your views here.
# from django.shortcuts import render_to_response
# from django.template import RequestContext
# from models import Store, Store_profile
# from common.ratings import get_rating_dict
# from common.models import Comment
# from mall.models import Mall_store


# def store_view( request, id, slug ):
#     store = Store.objects.get(id = id )
#     store_profile = Store_profile.objects.get( store = store )
#     images = store.get_image(single = False)
#     rating_dict = get_rating_dict( store, request )
#     print rating_dict
#     location = store.get_location()
#     reviews = Comment.get_comments( store )
#     context = {'entity': store, 'entity_profile': store_profile, 'images': images, 'rating_categories': rating_dict, \
#     			'reviews': reviews, 'location': location }
#     return render_to_response('common/store_detail.html', context, context_instance=RequestContext(request))


# def get_stores_summary( request, mall_id = None):
# 	entities = Store.objects.all()
# 	if mall_id:
# 		entities = [ ms.store for ms in Mall_store.objects.filter( mall__id = mall_id ) ]
# 	return render_to_response('common/list_entities.html', {'result_title': 'Store Listings', \
#         'entities': entities, 'selected': 'store' }, context_instance=RequestContext(request) )