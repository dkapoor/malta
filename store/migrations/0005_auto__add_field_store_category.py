# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Store.category'
        db.add_column(u'store_store', 'category',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['common.Category']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Store.category'
        db.delete_column(u'store_store', 'category_id')


    models = {
        u'common.category': {
            'Meta': {'object_name': 'Category'},
            'category_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.State']"})
        },
        u'common.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'common.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.image': {
            'Meta': {'object_name': 'Image'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'common.location': {
            'Meta': {'object_name': 'Location'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.City']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'common.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'common.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.timing': {
            'Meta': {'object_name': 'Timing'},
            'end_am_pm': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'end_hour': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'end_min': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_am_pm': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'start_hour': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'start_min': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'mall.mall': {
            'Meta': {'object_name': 'Mall'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Country']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'developer': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Location']", 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pincode': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'tags': ('tagging.fields.TagField', [], {}),
            'total_basements': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_floors': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_parking': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'total_sqft': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'store.event': {
            'Meta': {'object_name': 'Event'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Eventtype']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']", 'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'store.eventtype': {
            'Meta': {'object_name': 'Eventtype'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'store.mall_store': {
            'Meta': {'object_name': 'Mall_store'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Mall']"}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'area_sqft': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']"}),
            'contact_no': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'days_closed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'floor_no': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'manager_contact'", 'null': 'True', 'to': u"orm['common.Contact']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'owner_contact': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owner_contact'", 'null': 'True', 'to': u"orm['common.Contact']"}),
            'payment_method': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop_no': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tags': ('tagging.fields.TagField', [], {})
        },
        u'store.store_image': {
            'Meta': {'object_name': 'Store_image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_thumbnail': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"}),
            'store_image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']"})
        },
        u'store.store_offer': {
            'Meta': {'object_name': 'Store_offer'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'discount': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']", 'null': 'True', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'store.store_product': {
            'Meta': {'object_name': 'Store_product'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Product']"}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"})
        },
        u'store.store_timing': {
            'Meta': {'object_name': 'Store_timing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"}),
            'timing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Timing']"})
        }
    }

    complete_apps = ['store']