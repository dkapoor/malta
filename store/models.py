from django.db import models
from mall.models import Mall
from common.models import Category, Timing, Contact, Product, Image
from tagging.fields import TagField
import strings
from django.contrib.contenttypes.models import ContentType


class Store(models.Model):
    name = models.CharField( max_length = 100 )
    description = models.TextField()
    shop_no     = models.CharField( max_length = 20 )
    floor_no    = models.PositiveIntegerField()
    area_sqft   = models.PositiveIntegerField()
    category    = models.ForeignKey( Category )
    days_closed = models.CharField( max_length = 100 )
    payment_method = models.CharField( max_length = 100 )
    contact_no = models.CharField( max_length = 12 )
    owner_contact = models.ForeignKey( Contact, related_name = 'owner_contact', null = True )
    manager_contact = models.ForeignKey( Contact, related_name = 'manager_contact', null = True )
    tags        = TagField()

    def image_url(self):
        return Store_image.objects.filter( store = self, is_thumbnail = True )[0].store_image.get_image_url() \
        if Store_image.objects.filter( store = self, is_thumbnail = True ).exists() else strings.DEFAULT_STORE_IMAGE_URL

    def type_id(self):
        return ContentType.objects.get_for_model(self).id

    def brand_name(self):
        return self.brand_store_set.all()[0].brand.name if self.brand_store_set.exists() else None

    def location(self):
        return self.mall_store_set.all()[0].mall.location if self.mall_store_set.exists() else None 

    def mall(self):
        return self.mall_store_set.all()[0].mall if self.mall_store_set.exists() else None 

class Mall_store(models.Model):
    mall = models.ForeignKey( Mall )
    store = models.ForeignKey( Store )
        

class Store_image(models.Model):
    store = models.ForeignKey( Store )
    store_image = models.ForeignKey( Image )
    is_thumbnail = models.BooleanField( default = False )

class Store_timing(models.Model):
    store = models.ForeignKey( Store )
    timing = models.ForeignKey( Timing )


class Store_product(models.Model):
    store = models.ForeignKey( Store )
    product = models.ForeignKey( Product )

        
class Store_offer(models.Model):
    store = models.ForeignKey( Store )
    title = models.CharField( max_length = 100 )
    description = models.TextField()
    discount = models.PositiveIntegerField()
    image = models.ForeignKey( Image, null = True, blank = True )
    category = models.ForeignKey( Category, null = True, blank = True )
    is_active = models.BooleanField( default = True )

class Eventtype(models.Model):
    title = models.CharField(max_length = 100 )
    description = models.TextField()

    def __unicode__(self):
        return unicode(self.title)

class Event(models.Model):
    store = models.ForeignKey( Store )
    title = models.CharField( max_length = 100 )
    description = models.TextField()
    image = models.ForeignKey( Image, null = True, blank = True )
    category = models.ForeignKey( Category, null = True, blank = True )
    event_type = models.ForeignKey( Eventtype, null = True, blank = True )
    start_time = models.DateTimeField( null = True )
    end_time = models.DateTimeField( null = True )

    def type_id(self):
        return ContentType.objects.get_for_model(self).id