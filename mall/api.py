from tastypie.resources import ModelResource
from mall.models import *
from tastypie import fields
from tastypie.authentication import SessionAuthentication, ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization, Authorization
import os
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.http import HttpUnauthorized, HttpForbidden, HttpApplicationError
from tastypie.resources import Resource, ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.bundle import Bundle
from tastypie import fields
from tastypie.models import ApiKey, create_api_key
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import models
from common.models import Image, Contact, City, Country, Product, Activity
from common.api import TimingResource, ContactResource, ImageResource, urlencodeSerializer, CityResource, \
CountryResource, LocationResource, AdminApiKeyAuthentication
import ast

from log import get_request_logger
logger = get_request_logger()


class FloorPlanResource(ModelResource):
    plan_image = fields.ForeignKey( ImageResource, 'plan_image', full = True )
    class Meta:
        queryset = Floor_plan.objects.all()
        allowed_methods = ['get', 'post', 'delete']
        resource_name = 'floor_plan'
        filtering = {
            'id': 'exact'
        }

        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()

class MallResource(ModelResource):
    city = fields.ForeignKey( CityResource, 'city', full = True )
    country = fields.ForeignKey( CountryResource, 'country', full = True )
    location = fields.ForeignKey( LocationResource, 'location', null = True )
    type_id = fields.IntegerField( attribute = 'type_id' )

    class Meta:
        queryset = Mall.objects.all()
        limit = 0
        allowed_methods = ['get', 'post', 'delete']
        resource_name = 'mall'
        filtering = {
            'id': 'exact'
        }

        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
        serializer = urlencodeSerializer()

    def authorized_read_list(self, object_list, bundle):
        if object_list.count() == 1 and bundle.request.REQUEST.has_key('api_key'):
            user = ApiKey.objects.get( key = bundle.request.REQUEST['api_key']).user
            Activity.note_activity('VI', object_list[0], user = user )
        return super( MallResource, self ).authorized_read_list( object_list, bundle )


    def obj_create( self, bundle, request = None, **kwargs ):
        bundle = super( MallResource, self ).obj_create( bundle, request = request, **kwargs )
        mall = bundle.obj
        request = bundle.request
        contacts = request.REQUEST.get("mall_contacts", None)
        if contacts:  
            contacts = ast.literal_eval( contacts )              
            result = [ mc.delete() for mc in Mall_contact.objects.filter(mall = mall) ] if Mall_contact.objects.filter(mall = mall).exists() else None
            for name, number in contacts.items():
                contact = Contact( name = name.strip(), phone = number.strip() )
                contact.save()
                    
                mall_contact = Mall_contact(mall = mall, contact = contact )
                mall_contact.save()
        timing = request.REQUEST.get('mall_timing', None)
        
        if timing:
            timing = ast.literal_eval( timing )
            start_hour   = timing.get('start_hour', None)
            start_min   =  timing.get('start_min', None)
            end_hour   = timing.get('end_hour', None)
            end_min   = timing.get('end_min', None)
            start_am_pm   = timing.get('start_am_pm', None)
            end_am_pm   = timing.get('end_am_pm', None)
            
            old_mall_timings = Mall_timing.objects.filter( mall = mall )
            if old_mall_timings.exists():
                result = [ old_mall_timing.delete() for old_mall_timing in old_mall_timings ]
                
            timing, created = Timing.objects.get_or_create( start_hour = start_hour, start_min = start_min, \
                                                                end_hour = end_hour, end_min = end_min, start_am_pm = start_am_pm.upper(), end_am_pm = end_am_pm.upper() )

            mall_timing = Mall_timing.objects.create( mall = mall, timing = timing )

        fp_images_map = request.REQUEST.get('mall_fp_images_map', None )
        if fp_images_map:
            fp_images_map = ast.literal_eval( fp_images_map )
            result = [ mfps.delete() for mfps in Mall_floor_plan.objects.filter(mall = mall) ]
            for floor_no, image_id in fp_images_map.items():
                fp, created = Floor_plan.objects.get_or_create( floor_no = int(floor_no), plan_image = Image.objects.get(id = image_id) )
                mfp, created = Mall_floor_plan.objects.get_or_create( mall = mall, floor_plan = fp )

        mall_image_ids = request.REQUEST.get( 'mall_image_ids', None )
        thumbnail_image_id = request.REQUEST.get( 'thumbnail_image_id', 0 )
        if mall_image_ids:
            mall_image_ids = ast.literal_eval( mall_image_ids )
            images = Image.objects.filter( id__in = mall_image_ids )
            mall_images = Mall_image.objects.filter( mall = mall )
                
            for mi in mall_images:
                mi.mall_image.delete()
                mi.delete()

            for image in images:
                mall_image = Mall_image( mall = mall, mall_image = image )
                mall_image.is_thumbnail = (image.id == int( thumbnail_image_id ) )
                mall_image.save()



        nearby_mall_ids = request.REQUEST.get( 'mall_nearby_mall_ids', None )
            
        new_nearby_malls = []
        if nearby_mall_ids:
            nearby_mall_ids = ast.literal_eval( nearby_mall_ids )
            malls_nearby = Mall.objects.filter( id__in = nearby_mall_ids )
            old_nearby = Mall_nearby.objects.filter( mall = mall )
            for onear in old_nearby:
                onear.delete()
            for mall_nearby in malls_nearby:
                Mall_nearby.objects.create(mall = mall, nearby_mall = mall_nearby )
        return bundle


class MallTimingResource(ModelResource):
    mall = fields.ForeignKey( MallResource, 'mall' )
    timing = fields.ForeignKey( TimingResource, 'timing', full = True )
    
    class Meta:
        queryset = Mall_timing.objects.all()
        allowed_methods = ['get']
        resource_name = 'mall_timing'
        filtering = {
            'id': 'exact',
            'mall': ALL_WITH_RELATIONS
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class MallContactResource(ModelResource):
    mall = fields.ForeignKey( MallResource, 'mall' )
    contact = fields.ForeignKey( ContactResource, 'contact', full = True )
    
    class Meta:
        queryset = Mall_contact.objects.all()
        allowed_methods = ['get']
        resource_name = 'mall_contact'
        filtering = {
            'id': 'exact',
            'mall': ALL_WITH_RELATIONS
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class MallFloorPlanResource(ModelResource):
    mall = fields.ForeignKey( MallResource, 'mall' )
    floor_plan = fields.ForeignKey( FloorPlanResource, 'floor_plan', full = True )

    class Meta:
        queryset = Mall_floor_plan.objects.all()
        allowed_methods = ['get']
        resource_name = 'mall_floor_plan'
        filtering = {
            'id': 'exact',
            'mall': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class MallImageResource(ModelResource):
    mall = fields.ForeignKey( MallResource, 'mall' )
    mall_image = fields.ForeignKey( ImageResource, 'mall_image', full = True )

    class Meta:
        queryset = Mall_image.objects.all()
        allowed_methods = ['get']
        resource_name = 'mall_image'
        filtering = {
            'id': 'exact',
            'mall': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()


class MallNearbyResource(ModelResource):
    mall = fields.ForeignKey( MallResource, 'mall' )
    nearby_mall = fields.ForeignKey( MallResource, 'nearby_mall', full = True )

    class Meta:
        queryset = Mall_nearby.objects.all()
        allowed_methods = ['get']
        resource_name = 'nearby_mall'
        filtering = {
            'id': 'exact',
            'mall': ALL_WITH_RELATIONS,
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = ReadOnlyAuthorization()

class ProductResource(ModelResource):
    type_id = fields.IntegerField( attribute = 'type_id' )

    class Meta:
        queryset = Product.objects.all()
        allowed_methods = ['get', 'post', 'delete']
        resource_name = 'product'
        filtering = {
            'id': 'exact'
        }

        limit = 0
        always_return_data = True
        authentication = AdminApiKeyAuthentication()
        authorization = Authorization()
