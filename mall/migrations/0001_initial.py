# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Mall'
        db.create_table(u'mall_mall', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('zone', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.City'])),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Country'])),
            ('pincode', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('developer', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('total_sqft', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('total_floors', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('total_basements', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('total_parking', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('tags', self.gf('tagging.fields.TagField')()),
        ))
        db.send_create_signal(u'mall', ['Mall'])

        # Adding model 'Mall_timing'
        db.create_table(u'mall_mall_timing', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mall', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mall.Mall'])),
            ('timing', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Timing'])),
        ))
        db.send_create_signal(u'mall', ['Mall_timing'])

        # Adding model 'Mall_contact'
        db.create_table(u'mall_mall_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mall', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mall.Mall'])),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Contact'])),
        ))
        db.send_create_signal(u'mall', ['Mall_contact'])

        # Adding model 'Floor_plan'
        db.create_table(u'mall_floor_plan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('floor_no', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('plan_image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Image'])),
        ))
        db.send_create_signal(u'mall', ['Floor_plan'])

        # Adding model 'Mall_floor_plan'
        db.create_table(u'mall_mall_floor_plan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mall', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mall.Mall'])),
            ('floor_plan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mall.Floor_plan'], null=True)),
        ))
        db.send_create_signal(u'mall', ['Mall_floor_plan'])

        # Adding model 'Mall_nearby'
        db.create_table(u'mall_mall_nearby', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mall', self.gf('django.db.models.fields.related.ForeignKey')(related_name='mall_id', to=orm['mall.Mall'])),
            ('nearby_mall', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='nearby_malls', null=True, to=orm['mall.Mall'])),
        ))
        db.send_create_signal(u'mall', ['Mall_nearby'])

        # Adding model 'Mall_image'
        db.create_table(u'mall_mall_image', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mall', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mall.Mall'])),
            ('mall_image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Image'])),
        ))
        db.send_create_signal(u'mall', ['Mall_image'])

        # Adding model 'Mall_offer'
        db.create_table(u'mall_mall_offer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mall', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mall.Mall'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Image'], null=True, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Category'], null=True, blank=True)),
        ))
        db.send_create_signal(u'mall', ['Mall_offer'])


    def backwards(self, orm):
        # Deleting model 'Mall'
        db.delete_table(u'mall_mall')

        # Deleting model 'Mall_timing'
        db.delete_table(u'mall_mall_timing')

        # Deleting model 'Mall_contact'
        db.delete_table(u'mall_mall_contact')

        # Deleting model 'Floor_plan'
        db.delete_table(u'mall_floor_plan')

        # Deleting model 'Mall_floor_plan'
        db.delete_table(u'mall_mall_floor_plan')

        # Deleting model 'Mall_nearby'
        db.delete_table(u'mall_mall_nearby')

        # Deleting model 'Mall_image'
        db.delete_table(u'mall_mall_image')

        # Deleting model 'Mall_offer'
        db.delete_table(u'mall_mall_offer')


    models = {
        u'common.category': {
            'Meta': {'object_name': 'Category'},
            'category_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.State']"})
        },
        u'common.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'common.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.image': {
            'Meta': {'object_name': 'Image'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'common.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'common.timing': {
            'Meta': {'object_name': 'Timing'},
            'end_am_pm': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'end_hour': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'end_min': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_am_pm': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'start_hour': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'start_min': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'mall.floor_plan': {
            'Meta': {'object_name': 'Floor_plan'},
            'floor_no': ('django.db.models.fields.PositiveIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'plan_image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']"})
        },
        u'mall.mall': {
            'Meta': {'object_name': 'Mall'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Country']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'developer': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pincode': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'tags': ('tagging.fields.TagField', [], {}),
            'total_basements': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_floors': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_parking': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'total_sqft': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mall.mall_contact': {
            'Meta': {'object_name': 'Mall_contact'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Contact']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Mall']"})
        },
        u'mall.mall_floor_plan': {
            'Meta': {'object_name': 'Mall_floor_plan'},
            'floor_plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Floor_plan']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Mall']"})
        },
        u'mall.mall_image': {
            'Meta': {'object_name': 'Mall_image'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Mall']"}),
            'mall_image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']"})
        },
        u'mall.mall_nearby': {
            'Meta': {'object_name': 'Mall_nearby'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mall_id'", 'to': u"orm['mall.Mall']"}),
            'nearby_mall': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'nearby_malls'", 'null': 'True', 'to': u"orm['mall.Mall']"})
        },
        u'mall.mall_offer': {
            'Meta': {'object_name': 'Mall_offer'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Category']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Image']", 'null': 'True', 'blank': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Mall']"}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'mall.mall_timing': {
            'Meta': {'object_name': 'Mall_timing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mall.Mall']"}),
            'timing': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Timing']"})
        }
    }

    complete_apps = ['mall']