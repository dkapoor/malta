# from django import forms
# from django.shortcuts import render_to_response
# from django.contrib.auth.decorators import login_required
# from django.http import HttpResponse
# from django.shortcuts import render_to_response
# from django.template import RequestContext
# from models import Mall, Mall_profile
# from common.ratings import get_rating_dict
# from common.models import Comment, Activity

# def main( request ):

#     activities = Activity.get_latest_activity()
#     context = {'selected': 'home','activities': activities}
#     return render_to_response('common/main_home.html', context, context_instance=RequestContext(request))

# def mall_view( request, id, slug ):
#     mall_profile = Mall_profile.objects.get( mall__id = id )
#     mall = mall_profile.mall
#     images = mall_profile.get_image(single = False)
#     reviews = Comment.get_comments( mall )
#     rating_dict = get_rating_dict( mall, request )
#     location = mall_profile.get_location()

#     context = {'entity_profile': mall_profile, 'entity': mall, 'images': images, \
#                 'rating_categories': rating_dict, 'reviews': reviews, 'location': location }
#     return render_to_response('common/mall_detail.html', context, context_instance=RequestContext(request))

# def get_malls_summary( request ):
#     entities = Mall_profile.objects.all()
#     return render_to_response('common/list_entities.html', {'result_title': 'Malls Listings', \
#         'entities': entities, 'selected': 'mall' }, context_instance=RequestContext(request) )
#     