from django.db import models
from common.models import Timing, Contact, Image, City, Country, Category, Location
from tagging.fields import TagField
import strings
from django.contrib.contenttypes.models import ContentType


class Mall(models.Model):
    CITY_CHOICES = (
                    ('DEL', 'Delhi'),
                    ('NCR', 'NCR'),
                    ('MUM', 'Mumbai'),
                )

    COUNTRY_CHOICES = (
                ('IND', 'India'),
        )

    name = models.CharField( max_length = 100 )
    description = models.TextField()
    address = models.TextField()
    location = models.ForeignKey( Location, null = True )
    zone = models.CharField( max_length = 100 )
    city = models.ForeignKey( City )
    country = models.ForeignKey( Country )
    pincode = models.CharField( max_length = 6 )
    developer = models.CharField( max_length = 100, null = True, blank = True )
    total_sqft = models.PositiveIntegerField()
    total_floors = models.PositiveIntegerField()
    total_basements = models.PositiveIntegerField()
    total_parking   = models.CharField( max_length = 5, null = True, blank = True )
    website         = models.URLField( null = True, blank = True )
    tags            = TagField()

    def __unicode__(self):
        return unicode(self.name)

    def image_url(self):
        return Mall_image.objects.filter( mall = self, is_thumbnail = True )[0].image.get_image_url() \
        if Mall_image.objects.filter( mall = self, is_thumbnail = True ).exists() else strings.DEFAULT_MALL_IMAGE_URL

    def type_id(self):
        return ContentType.objects.get_for_model(self).id

class Mall_timing(models.Model):
    mall = models.ForeignKey( Mall )
    timing = models.ForeignKey( Timing )
        

class Mall_contact(models.Model):
    mall = models.ForeignKey( Mall )
    contact = models.ForeignKey( Contact )


class Floor_plan(models.Model):
    floor_no = models.PositiveIntegerField()
    plan_image = models.ForeignKey( Image )

class Mall_floor_plan(models.Model):
    mall = models.ForeignKey( Mall )
    floor_plan = models.ForeignKey( Floor_plan, null = True )
        

class Mall_nearby(models.Model):
    mall = models.ForeignKey( Mall, related_name = 'mall_id' )
    nearby_mall = models.ForeignKey( Mall, null = True, blank = True, related_name = 'nearby_malls' )


class Mall_image(models.Model):
    mall = models.ForeignKey( Mall )
    mall_image = models.ForeignKey( Image )
    is_thumbnail = models.BooleanField( default = False )

        
class Mall_offer(models.Model):
    mall = models.ForeignKey( Mall )
    title = models.CharField( max_length = 100 )
    description = models.TextField()
    image = models.ForeignKey( Image, null = True, blank = True )
    start_date = models.DateField()
    end_date = models.DateField()
    category = models.ForeignKey( Category, null = True, blank = True )
        
 
        
