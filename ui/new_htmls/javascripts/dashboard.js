﻿$(".fileUploadWidget img").click(function () {
     $(this).prev().click();

    // alert("some");
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).next().attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".uploadFile").change(function () {
    readURL(this);
});
$(".fileUploadWidget .removeImg").click(function () {
    $(this).prev().attr('src', "images/noImg.png");
    }
    );