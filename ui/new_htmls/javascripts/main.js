$(function () {
   

    $('.selectpicker').selectpicker();
    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
 setInterval(function(){
     $(".bannerWidget").fadeToggle();

     $(".bWStoreAutoSlider").fadeToggle();
     

 },3000);
 
 $(".bWHoverSlider").mouseover(function () {
   
 });
});
$("#oneTheMap").click(function () {
    var content = $(this).html();
    if (content == "ON THE MAP") {
        $(this).text("BACK TO IMAGES").css("width", "156px");
    }
    else
        $(this).text("ON THE MAP").css("width","125px");
    $(".slider").toggleClass("mapViewSlider");
    $(".mapView").toggleClass("mapViewSlider");
    
});
$("#selectLoc").click(function () {
    $(".leftFilters").addClass("hideFilter");
    $(".filterArrows").removeClass("filterOpen");
    $("#filterLoc").toggleClass("hideFilter");
    $("#filterArrowLoc").addClass("filterOpen");

});
$("#selectMall").click(function () {
    $(".leftFilters").addClass("hideFilter");
    $(".filterArrows").removeClass("filterOpen");
    $("#filterMall").toggleClass("hideFilter");
    $("#filterArrowMall").toggleClass("filterOpen");
});
$("#selectCat").click(function () {
    $(".leftFilters").addClass("hideFilter");
    $(".filterArrows").removeClass("filterOpen");
    $("#filterCat").toggleClass("hideFilter");
    $("#filterArrowCat").toggleClass("filterOpen");
});
$("#selectBrand").click(function () {
    $(".leftFilters").addClass("hideFilter");
    $(".filterArrows").removeClass("filterOpen");
    $("#filterBrand").toggleClass("hideFilter");
    $("#filterArrowBrand").toggleClass("filterOpen");
});
$(".storeMapFloors").click(function () {
    $(".storeMapFloors").css("background", "#282828");
    $(this).css("background", "#CC0202");
});