from django.conf.urls import patterns, include, url
from django.contrib.auth.views import logout
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib import admin
admin.autodiscover()
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from tastypie.api import Api
from common.api import ImageResource, UserResource, CountryResource, \
StateResource, CityResource, CategoryResource, ProductResource, TimingResource, ContactResource, UserProfileResource, \
LocationResource, EntityRatingCategoryMapResource, RatingCategoryResource, RatingVoteResource, CommentResource, ActivityResource
from mall.api import FloorPlanResource, MallResource, MallImageResource, MallNearbyResource, \
MallFloorPlanResource, FloorPlanResource, MallContactResource, MallTimingResource
from store.api import StoreResource, MallStoreResource, StoreImageResource, StoreTimingResource, \
StoreProductResource, StoreOfferResource, EventTypeResource, EventResource, SearchResource
from brand.api import BrandResource, BrandProductResource, BrandStoreResource, BrandImageResource

v1_api = Api(api_name = 'v1')

v1_api.register(MallImageResource())
v1_api.register(MallNearbyResource())
v1_api.register(MallFloorPlanResource())
v1_api.register(FloorPlanResource())
v1_api.register(MallContactResource())
v1_api.register(MallTimingResource())
v1_api.register(ImageResource())
v1_api.register(UserResource())
v1_api.register(MallResource())
v1_api.register(ProductResource())
v1_api.register(CountryResource())
v1_api.register(StateResource())
v1_api.register(CityResource())
v1_api.register(CategoryResource())
v1_api.register(StoreResource())
v1_api.register(MallStoreResource())
v1_api.register(StoreImageResource())
v1_api.register(StoreTimingResource())
v1_api.register(UserProfileResource())
v1_api.register(StoreProductResource())
v1_api.register(StoreOfferResource())
v1_api.register(EventTypeResource())
v1_api.register(TimingResource())
v1_api.register(BrandResource())
v1_api.register(BrandStoreResource())
v1_api.register(BrandProductResource())
v1_api.register(EventResource())
v1_api.register(ContactResource())
v1_api.register(SearchResource())
v1_api.register(LocationResource())
v1_api.register(RatingCategoryResource())
v1_api.register(EntityRatingCategoryMapResource())
v1_api.register(RatingVoteResource())
v1_api.register(CommentResource())
v1_api.register(ActivityResource())
v1_api.register(BrandImageResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'malta.views.home', name='home'),
    # url(r'^malta/', include('malta.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    # url(r'^main/', 'mall.views.main', name = "home"),
    # url(r'^malls/', 'mall.views.get_malls_summary', name = "mall_list"),
    # url(r'^stores/', 'store.views.get_stores_summary', name = "store_list"),
    # url(r'^create/(?P<entity_type>[mall,store]{4,5})', 'common.views.create', name = "create"),
    # (r'^accounts/', include('allauth.urls')),
    # (r'^search/', include('haystack.urls')),
    # url(r'^post_comment/', 'common.views.post_comment', name = "post_comment"),
     (r'^admin/', include(admin.site.urls)),
     url(r'^login/', 'django.contrib.auth.views.login', {'template_name': 'login.html' }),
     (r'^logout/', logout, {'next_page': '/main/'}),
    # url(r'^post-login/', 'common.views.post_login', name = 'post-login'),
    # url(r'^signup/', 'common.views.sign_up', name = 'signup'),
    # url(r'^vote/(?P<map_id>[0-9]+)/(?P<ctype_id>[0-9]+)/(?P<obj_id>[0-9]+)/(?P<rating>[0-9]+)/$', 'common.views.put_rating', name = 'vote' ),
    # url(r'^store/(?P<id>[0-9]+)/(?P<slug>[-_\w]+)/$', 'store.views.store_view', name = 'store_view'),
    # url(r'^mall/(?P<id>[0-9]+)/(?P<slug>[-_\w]+)/$', 'mall.views.mall_view', name = 'mall_view'),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
) + staticfiles_urlpatterns()